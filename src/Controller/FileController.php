<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\LoadFileForm;
use App\Service\FileImporterServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FileController extends AbstractController
{
    #[Route('/file/import', 'import_file')]
    public function importFile(
        Request $request,
        FileImporterServiceInterface $fileImporterService
    ): Response {
        $form = $this->createForm(LoadFileForm::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formFile = $form->get('file')->getData();

            if ($formFile) {
                $fileImporterService->import($formFile);
            }
        }

        return $this->render('file/importFile.html.twig', [
            'form' => $form
        ]);
    }
}
