<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Repository\ProductRepositoryInterface;
use App\Service\Query\DataTableServiceInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractApiController
{
    #[Route('/api/product', 'get_products_list')]
    public function getListAction(
        Request $request,
        DataTableServiceInterface $dataTableService,
        ProductRepositoryInterface $productRepository
    ): Response {
        return $this->getList($request, $dataTableService, $productRepository);
    }
}
