<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\DTO\DTOInterface;
use App\Repository\DataTableRepositoryInterface;
use App\Service\Query\DataTableServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractApiController extends AbstractController
{
    protected function getList(
        Request $request,
        DataTableServiceInterface $dataTableService,
        DataTableRepositoryInterface $dataTableRepository
    ): JsonResponse {
        $parameters = $dataTableService->parseFromQuery($request->query->all());
        $data = $dataTableRepository->searchByParameters($parameters);
        $count = $dataTableRepository->countByParameters($parameters);

        return new JsonResponse([
            'data' => array_map(function (DTOInterface $productDto) {
                return $productDto->toArray();
            }, $data),
            'draw' => $parameters->draw,
            'recordsTotal' => $count,
            'recordsFiltered' => $count
        ]);
    }
}
