<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Repository\CategoryRepositoryInterface;
use App\Service\Query\DataTableServiceInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryRepository extends AbstractApiController
{
    #[Route('/api/category', 'get_categories_list')]
    public function getListAction(
        Request $request,
        DataTableServiceInterface $dataTableService,
        CategoryRepositoryInterface $categoryRepository
    ): Response {
        return $this->getList($request, $dataTableService, $categoryRepository);
    }
}
