<?php

declare(strict_types=1);

namespace App\DTO;

final readonly class CategoryDTO implements DTOInterface
{
    public function __construct(
        private int $id,
        private string $name
    ) {
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}
