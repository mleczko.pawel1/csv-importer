<?php

declare(strict_types=1);

namespace App\Command;

use App\Exception\FileNotFoundException;
use App\Service\FileOperatorInterface;
use App\Service\FileDbImporterInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:file:load',
    description: 'Command parses imported file'
)]
class FileLoaderCommand extends Command
{
    public function __construct(
        private readonly FileOperatorInterface $fileLoader,
        private readonly FileDbImporterInterface $fileParser
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $fileData = $this->fileLoader->loadFile();
        } catch (FileNotFoundException) {
            $output->writeln('File not exists');
            return Command::FAILURE;
        }

        $this->fileParser->parse($fileData);
        $output->writeln('File loaded to DB');

        try {
            $this->fileLoader->removeFile();
            $output->writeln('File removed');
        } catch (FileNotFoundException) {
            $output->writeln('File not exists');
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
