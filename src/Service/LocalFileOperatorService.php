<?php

declare(strict_types=1);

namespace App\Service;

use App\Exception\FileNotFoundException;

final readonly class LocalFileOperatorService implements FileOperatorInterface, FileOperationInterface
{
    public const FILE_EXTENSION = 'csv';

    public function __construct(
        private string $defaultPath
    ) {
    }

    /**
     * @throws FileNotFoundException
     */
    public function loadFile(): array
    {
        $filePath = $this->prepareFullFilePath();

        if (! file_exists($filePath)) {
            throw new FileNotFoundException($filePath);
        }

        if (($file = fopen($filePath, "r")) === false) {
            return [];
        }

        $csvData = [];
        while (($data = fgetcsv($file, 1000, ";")) !== false) {
            $csvData[] = $data;
        }

        fclose($file);

        return $csvData;
    }

    /**
     * @throws FileNotFoundException
     */
    public function removeFile(): void
    {
        $filePath = $this->prepareFullFilePath();

        if (! file_exists($filePath)) {
            throw new FileNotFoundException($filePath);
        }

        unlink($filePath);
    }

    private function prepareFullFilePath(): string
    {
        return sprintf(
            '%s/%s/%s.%s',
            $this->defaultPath,
            self::UPLOADS_DIR,
            self::IMPORTED_FILE_NAME,
            self::FILE_EXTENSION
        );
    }
}
