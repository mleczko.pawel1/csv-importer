<?php

declare(strict_types=1);

namespace App\Service;

interface FileOperationInterface
{
    public const UPLOADS_DIR = 'var/uploads';
    public const IMPORTED_FILE_NAME = 'fileToDb';
}
