<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface FileImporterServiceInterface
{
    public function import(UploadedFile $file): void;
}
