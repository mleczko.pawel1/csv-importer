<?php

declare(strict_types=1);

namespace App\Service;

use App\Exception\FileCouldNotMoveException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class LocalFileImporterService implements FileImporterServiceInterface, FileOperationInterface
{
    public function __construct(
        private readonly string $projectPath
    ) {
    }

    public function import(UploadedFile $file): void
    {
        $newFileName = $this->prepareNewFileName($file);

        try {
            $file->move(
                $this->prepareMoveDirectoryPath(),
                $newFileName
            );
        } catch (FileException) {
            throw new FileCouldNotMoveException();
        }
    }

    private function prepareNewFileName(UploadedFile $file): string
    {
        $originalExtension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

        return sprintf('%s.%s', self::IMPORTED_FILE_NAME, $originalExtension);
    }

    private function prepareMoveDirectoryPath(): string
    {
        return sprintf('%s/%s', $this->projectPath, self::UPLOADS_DIR);
    }
}
