<?php

declare(strict_types=1);

namespace App\Service\Query;

class DataTableService implements DataTableServiceInterface
{
    private const DEFAULT_LIMIT = 10;

    public function parseFromQuery(array $parameters): DataTableParameters
    {
        $offset = (int) $parameters['start'] ?? 0;
        $limit = (int) $parameters['length'] ?? self::DEFAULT_LIMIT;
        $draw = (int) $parameters['draw'] ?? 1;

        $searchPhrase = $parameters['search']['value'] ?? null;

        return new DataTableParameters(
            $limit,
            $offset,
            $draw,
            $searchPhrase
        );

    }
}
