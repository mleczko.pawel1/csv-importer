<?php

declare(strict_types=1);

namespace App\Service\Query;

interface DataTableServiceInterface
{
    public function parseFromQuery(array $parameters): DataTableParameters;
}
