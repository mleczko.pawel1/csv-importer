<?php

declare(strict_types=1);

namespace App\Service\Query;

readonly class DataTableParameters
{
    public function __construct(
        public int $limit,
        public int $offset,
        public int $draw,
        public ?string $searchPhrase
    ) {
    }
}
