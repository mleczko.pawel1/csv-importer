<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;
use Psr\Log\LoggerInterface;

final class FileDbImporter implements FileDbImporterInterface
{
    private const LOG_MESSAGE = 'Product %s already exists!';

    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly ProductRepositoryInterface $productRepository
    ) {
    }

    public function parse(array $fileData): void
    {
        foreach ($fileData as $row) {
            [$productName, $productId] = $row;
            $productId = (int) $productId;

            $product = $this->productRepository->findById($productId);

            if (! $product) {
                $product = new Product($productId, $productName);
                $this->productRepository->save($product);
            } else {
                $this->logger->info(sprintf(self::LOG_MESSAGE, $productName));
            }
        }
    }
}
