<?php

declare(strict_types=1);

namespace App\Service;

interface FileDbImporterInterface
{
    public function parse(array $fileData): void;
}
