<?php

declare(strict_types=1);

namespace App\Service;

use App\Exception\FileNotFoundException;

interface FileOperatorInterface
{
    /**
     * @throws FileNotFoundException
     */
    public function loadFile(): array;
    /**
     * @throws FileNotFoundException
     */
    public function removeFile(): void;
}
