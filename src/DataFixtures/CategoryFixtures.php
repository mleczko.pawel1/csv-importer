<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    private const FIXTURE_NAME = 'category fixture %d';

    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 100; $i++) {
            $category = new Category();
            $category->setName(sprintf(self::FIXTURE_NAME, $i));

            $manager->persist($category);
        }

        $manager->flush();
    }
}
