<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ProductRepositoryInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductRepositoryInterface::class)]
#[ORM\Table(name: 'products')]
final class Product
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 8, unique: true)]
        private readonly int $id,
        #[ORM\Column(length: 128)]
        private readonly string $name
    ) {
    }
}
