<?php

declare(strict_types=1);

namespace App\Repository;

use App\DTO\ProductDTO;
use App\Entity\Product;
use App\Service\Query\DataTableParameters;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class ProductRepository extends ServiceEntityRepository implements ProductRepositoryInterface, DataTableRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findById(int $id): ?Product
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder->select('c')
            ->from($this->getClassName(), 'c')
            ->where($queryBuilder->expr()->eq('c.id', ':productId'))
            ->setParameter(':productId', $id);

        return $queryBuilder->getQuery()
            ->getOneOrNullResult();
    }

    public function save(Product $product): void
    {
        $this->getEntityManager()->persist($product);
        $this->getEntityManager()->flush();
    }

    public function searchByParameters(DataTableParameters $parameters): array
    {
        $dtoSelect = str_replace(
            ':DTO',
            ProductDTO::class,
            "NEW :DTO(" .
            "p.id," .
            "p.name" .
            ")"
        );

        $queryBuilder = $this->prepareSelect($parameters, $dtoSelect);

        $queryBuilder->setFirstResult($parameters->offset)
            ->setMaxResults($parameters->limit);

        return $queryBuilder->getQuery()
            ->getResult();
    }

    public function countByParameters(DataTableParameters $parameters): int
    {
        $select = 'COUNT(p) as count';
        $queryBuilder = $this->prepareSelect($parameters, $select);

        try {
            return (int) $queryBuilder->getQuery()->getSingleScalarResult();
        } catch (NoResultException) {
            return 0;
        }
    }

    private function prepareSelect(DataTableParameters $parameters, string $select): QueryBuilder
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select($select)
            ->from($this->getEntityName(), 'p');

        if ($parameters->searchPhrase) {
            $queryBuilder->where($queryBuilder->expr()->like('p.name', "'%" . $parameters->searchPhrase . "%'"));
        }

        return $queryBuilder;
    }
}
