<?php

declare(strict_types=1);

namespace App\Repository;

use App\Service\Query\DataTableParameters;

interface DataTableRepositoryInterface
{
    public function searchByParameters(DataTableParameters $parameters): array;

    public function countByParameters(DataTableParameters $parameters): int;
}
