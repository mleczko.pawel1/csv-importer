<?php

declare(strict_types=1);

namespace App\Repository;

use App\DTO\CategoryDTO;
use App\Entity\Category;
use App\Service\Query\DataTableParameters;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class CategoryRepository extends ServiceEntityRepository implements CategoryRepositoryInterface, DataTableRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function save(Category $category): void
    {
        $this->getEntityManager()->persist($category);
        $this->getEntityManager()->flush();
    }

    public function searchByParameters(DataTableParameters $parameters): array
    {
        $dtoSelect = str_replace(
            ':DTO',
            CategoryDTO::class,
            "NEW :DTO(" .
            "c.id," .
            "c.name" .
            ")"
        );

        $queryBuilder = $this->prepareSelect($parameters, $dtoSelect);

        $queryBuilder->setFirstResult($parameters->offset)
            ->setMaxResults($parameters->limit);

        return $queryBuilder->getQuery()
            ->getResult();
    }

    public function countByParameters(DataTableParameters $parameters): int
    {
        $select = 'COUNT(c) as count';
        $queryBuilder = $this->prepareSelect($parameters, $select);

        try {
            return (int) $queryBuilder->getQuery()->getSingleScalarResult();
        } catch (NoResultException) {
            return 0;
        }
    }

    private function prepareSelect(DataTableParameters $parameters, string $select): QueryBuilder
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select($select)
            ->from($this->getEntityName(), 'c');

        if ($parameters->searchPhrase) {
            $queryBuilder->where($queryBuilder->expr()->like('c.name', "'%" . $parameters->searchPhrase . "%'"));
        }

        return $queryBuilder;
    }
}
