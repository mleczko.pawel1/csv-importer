FROM php:8.2-apache

RUN a2enmod rewrite

RUN apt update && \
    apt install -y git unzip zlib1g-dev libicu-dev g++ npm --no-install-recommends && \
    docker-php-ext-configure intl && \
    docker-php-ext-install bcmath pdo pdo_mysql intl && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY docker/apache.conf /etc/apache2/sites-enabled/000-default.conf
COPY docker/entrypoint.sh /entrypoint.sh
COPY . /app

RUN chmod +x /entrypoint.sh

WORKDIR /app

CMD ["apache2-foreground"]
ENTRYPOINT ["/entrypoint.sh"]
