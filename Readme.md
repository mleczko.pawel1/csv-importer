1. Aby odpalić projekt należy uruchomić komendę `docker-compose up -d` w katalogu z projektem. Automatycznie pobierze się obraz, oraz wykonają się podstawowe czynności, w tym załadowanie Fixtures.
2. Logowanie do wrzucania pliku: 
- user: john_admin
- pass: zaqzaq
3. SQL
- ```sql
SELECT COUNT(f.film_id) as film_count, distinct l.name as language_name 
FROM film f
LEFT JOIN language l ON f.language_id = l.language_id
``` 
- ```sql
SELECT COUNT(f.film_id) as film_count, distinct l.name as language_name 
FROM film f
LEFT JOIN language l ON f.language_id = l.language_id
WHERE film_count > 5
```