#!/usr/bin/env bash

composer install -n
bin/console doctrine:schema:update --force
bin/console doctrine:fixtures:load --no-interaction
npm install --force

exec "$@"