<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Exception\FileNotFoundException;
use App\Service\LocalFileOperatorService;
use PHPUnit\Framework\TestCase;

class LocalFileOperatorServiceTest extends TestCase
{
    private const TEST_DEFAULT_PATH = __DIR__ . '/../..';
    private const TEST_FILE_NAME = 'fileToDb';

    private string $testFilePath;

    private LocalFileOperatorService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->testFilePath = sprintf(
            '%s/var/uploads/%s.%s',
            self::TEST_DEFAULT_PATH,
            self::TEST_FILE_NAME,
            LocalFileOperatorService::FILE_EXTENSION
        );

        $this->service = new LocalFileOperatorService(self::TEST_DEFAULT_PATH);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        if (file_exists($this->testFilePath)) {
            unlink($this->testFilePath);
        }
    }

    public function testLoadFile(): void
    {
        $testData = [['foo', 'bar'], ['baz', 'qux']];
        $this->writeTestCsvFile($testData);

        $loadedData = $this->service->loadFile();

        $this->assertEquals($testData, $loadedData);
    }

    public function testLoadFileThrowsFileNotFoundException(): void
    {
        $this->expectException(FileNotFoundException::class);

        $this->service->loadFile();
    }

    public function testRemoveFile(): void
    {
        $this->writeTestCsvFile([]);

        $this->assertTrue(file_exists($this->testFilePath));

        $this->service->removeFile();

        $this->assertFalse(file_exists($this->testFilePath));
    }

    public function testRemoveFileThrowsFileNotFoundException(): void
    {
        $this->expectException(FileNotFoundException::class);

        $this->service->removeFile();
    }

    private function writeTestCsvFile(array $data): void
    {
        $file = fopen($this->testFilePath, 'w');

        foreach ($data as $row) {
            fputcsv($file, $row, ';');
        }

        fclose($file);
    }
}
