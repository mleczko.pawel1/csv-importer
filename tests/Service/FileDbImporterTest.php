<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;
use App\Service\FileDbImporter;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class FileDbImporterTest extends TestCase
{
    public function testParse(): void
    {
        $loggerMock = $this->createMock(LoggerInterface::class);
        $productRepositoryMock = $this->createMock(ProductRepositoryInterface::class);

        $fileData = [
            ['Product A', '1'],
            ['Product B', '2'],
            ['Product C', '3'],
        ];

        $productA = new Product(1, 'Product A');

        $productRepositoryMock
            ->expects($this->exactly(3))
            ->method('findById')
            ->withConsecutive([1], [2], [3])
            ->willReturnOnConsecutiveCalls($productA, null, null);

        $productRepositoryMock
            ->expects($this->exactly(2))
            ->method('save')
            ->withConsecutive([new Product(2, 'Product B')], [new Product(3, 'Product C')]);

        $loggerMock
            ->expects($this->once())
            ->method('info')
            ->with('Product Product A already exists!');

        $importer = new FileDbImporter($loggerMock, $productRepositoryMock);
        $importer->parse($fileData);
    }
}

