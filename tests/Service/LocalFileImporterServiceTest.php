<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Exception\FileCouldNotMoveException;
use App\Service\LocalFileImporterService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class LocalFileImporterServiceTest extends TestCase
{
    private const PROJECT_PATH = '/path/to/project';
    private const UPLOADS_DIR = 'var/uploads';
    private const IMPORTED_FILE_NAME = 'fileToDb';

    public function testImport(): void
    {
        $file = $this->createMock(UploadedFile::class);
        $file
            ->expects($this->once())
            ->method('getClientOriginalName')
            ->willReturn('file.csv');

        $file
            ->expects($this->once())
            ->method('move')
            ->with(self::PROJECT_PATH . '/' . self::UPLOADS_DIR, self::IMPORTED_FILE_NAME . '.csv');

        $importer = new LocalFileImporterService(self::PROJECT_PATH);
        $importer->import($file);
    }

    public function testImportThrowsFileCouldNotMoveException(): void
    {
        $file = $this->createMock(UploadedFile::class);
        $file
            ->expects($this->once())
            ->method('getClientOriginalName')
            ->willReturn('file.csv');

        $file
            ->expects($this->once())
            ->method('move')
            ->willThrowException(new FileException());

        $importer = new LocalFileImporterService(self::PROJECT_PATH);

        $this->expectException(FileCouldNotMoveException::class);

        $importer->import($file);
    }
}
