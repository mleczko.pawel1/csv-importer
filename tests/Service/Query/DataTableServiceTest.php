<?php

declare(strict_types=1);

namespace App\Tests\Service\Query;

use App\Service\Query\DataTableParameters;
use App\Service\Query\DataTableService;
use PHPUnit\Framework\TestCase;

class DataTableServiceTest extends TestCase
{
    private DataTableService $dataTableService;

    protected function setUp(): void
    {
        $this->dataTableService = new DataTableService();
    }

    public function testParseFromQuery(): void
    {
        $parameters = [
            'start' => 5,
            'length' => 10,
            'draw' => 2,
            'search' => [
                'value' => 'test',
            ],
        ];

        $result = $this->dataTableService->parseFromQuery($parameters);

        $this->assertInstanceOf(DataTableParameters::class, $result);
        $this->assertEquals(10, $result->limit);
        $this->assertEquals(5, $result->offset);
        $this->assertEquals(2, $result->draw);
        $this->assertEquals('test', $result->searchPhrase);
    }
}
