<?php

declare(strict_types=1);

namespace App\Tests\Service\Query;

use App\Service\Query\DataTableParameters;
use PHPUnit\Framework\TestCase;

class DataTableParametersTest extends TestCase
{
    public function testConstructor(): void
    {
        $limit = 10;
        $offset = 5;
        $draw = 2;
        $searchPhrase = 'test';

        $parameters = new DataTableParameters($limit, $offset, $draw, $searchPhrase);

        $this->assertInstanceOf(DataTableParameters::class, $parameters);
        $this->assertEquals($limit, $parameters->limit);
        $this->assertEquals($offset, $parameters->offset);
        $this->assertEquals($draw, $parameters->draw);
        $this->assertEquals($searchPhrase, $parameters->searchPhrase);
    }
}
